const mongoose = require('mongoose')
const express = require('express')
const router = express.Router()

router.use(express.urlencoded({extended:true}))
router.use(express.json())

const sessionModel = require('../models/sessionModel')


// Route Post/Create
router.post('/', async (req, res) => {
    const { date, dureeM, places, participations, cours_id, moniteur_id } = req.body;
    const nouvelleSession = new sessionModel({
        date: date,
        dureeM: dureeM,
        places: places,
        participations: participations,
        cours_id: cours_id,
        moniteur_id: moniteur_id
    });

    try {
        const savedSession = await nouvelleSession.save();
        res.status(201).send("Session créé");
    } catch (err) {
        res.status(400).send(err.message);
    }
});

// Route Get/Read
router.get('/:id', async (req, res) => {
    const sessionId = req.params.id;

    try {
        const session = await sessionModel.findById(sessionId);
        if (!session) {
            return res.status(404).json({ message: "Session not found" });
        }
        res.json(session);
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});






// Route Patch/Update
router.put('/:id', async (req, res) => {
    const sessionId = req.params.id;

    try {
        const updatedSession = await sessionModel.findByIdAndUpdate(sessionId, req.body, { new: true });
        if (!updatedSession) {
            return res.status(404).json({ message: "Session not found" });
        }
        res.json(updatedSession);
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});

// Route Delete
router.delete('/:id', async (req, res) => {
    const sessionId = req.params.id;

    try {
        const deletedSession = await sessionModel.findByIdAndDelete(sessionId);
        if (!deletedSession) {
            return res.status(404).json({ message: "Session not found" });
        }
        res.json({ message: "Session deleted" });
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});


module.exports = router