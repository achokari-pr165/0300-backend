const mongoose = require('mongoose')
const express = require('express')
const router = express.Router()

router.use(express.urlencoded({extended:true}))
router.use(express.json())

const coursModel = require('../models/coursModel')


// Route Post/Create
router.post('/', async (req, res) => {
    const { tarif, niveau, type } = req.body;
    const nouveauCours = new coursModel({
        tarif: tarif,
        niveau: niveau,
        type: type
    });

    try {
        const savedCours = await nouveauCours.save();
        res.status(201).send("Cours créé");
    } catch (err) {
        res.status(400).send(err.message);
    }
});

// Route Get/Read
router.get('/:id', async (req, res) => {
    const coursId = req.params.id;

    try {
        const cours = await coursModel.findById(coursId);
        if (!cours) {
            return res.status(404).json({ message: "Cours not found" });
        }
        res.json(cours);
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});






// Route Patch/Update
router.put('/:id', async (req, res) => {
    const coursId = req.params.id;

    try {
        const updatedCours = await coursModel.findByIdAndUpdate(coursId, req.body, { new: true });
        if (!updatedCours) {
            return res.status(404).json({ message: "Cours not found" });
        }
        res.json(updatedCours);
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});

// Route Delete
router.delete('/:id', async (req, res) => {
    const coursId = req.params.id;

    try {
        const deletedCours = await coursModel.findByIdAndDelete(coursId);
        if (!deletedCours) {
            return res.status(404).json({ message: "Cours not found" });
        }
        res.json({ message: "Cours deleted" });
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});


module.exports = router