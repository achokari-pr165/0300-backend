// /routes/proprietaire.js

const mongoose = require('mongoose');
const express = require('express');
const router = express.Router();

router.use(express.urlencoded({extended: true}));
router.use(express.json());

const proprietaireModel = require('../models/proprietaireModel');
const sessionModel = require('../models/sessionModel');

// Route Post/Create
router.post('/', async (req, res) => {
    const { adresse, certificatDelivre, dateNaissance, nom, origine, prenom, chiens, localite, npa, mails, telephones } = req.body;
    const nouveauProprietaire = new proprietaireModel({
        adresse: adresse,
        certificatDelivre: certificatDelivre,
        dateNaissance: dateNaissance,
        nom: nom,
        origine: origine,
        prenom: prenom,
        chiens: chiens,
        localite: localite,
        npa: npa,
        mails: mails,
        telephones: telephones,
    });

    try {
        const savedProprietaire = await nouveauProprietaire.save();
        res.status(201).send("Proprietaire créé");
    } catch (err) {
        res.status(400).send(err.message);
    }
});

// Route Get/Read
router.get('/:id', async (req, res) => {
    const proprietaireId = req.params.id;

    try {
        const proprietaire = await proprietaireModel.findById(proprietaireId);
        if (!proprietaire) {
            return res.status(404).json({ message: "Proprietaire not found" });
        }
        res.json(proprietaire);
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});

// Route Patch/Update
router.put('/:id', async (req, res) => {
    const proprietaireId = req.params.id;

    try {
        const updatedProprietaire = await proprietaireModel.findByIdAndUpdate(proprietaireId, req.body, { new: true });
        if (!updatedProprietaire) {
            return res.status(404).json({ message: "Proprietaire not found" });
        }
        res.json(updatedProprietaire);
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});

// Route Delete
router.delete('/:id', async (req, res) => {
    const proprietaireId = req.params.id;

    try {
        const deletedProprietaire = await proprietaireModel.findByIdAndDelete(proprietaireId);
        if (!deletedProprietaire) {
            return res.status(404).json({ message: "Proprietaire not found" });
        }
        res.json({ message: "Proprietaire deleted" });
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});

// Nouvelle Route: Durée Totale d'Entraînement par Propriétaire
router.get('/:id/totalTrainingHours', async (req, res) => {
    const proprietaireId = req.params.id;

    try {
        const result = await sessionModel.aggregate([
            {
                $unwind: "$participations"
            },
            {
                $lookup: {
                    from: "proprietaires",
                    localField: "participations.chien_id",
                    foreignField: "chiens._id",
                    as: "proprietaire_details"
                }
            },
            {
                $unwind: "$proprietaire_details"
            },
            {
                $match: {
                    "proprietaire_details._id": proprietaireId
                }
            },
            {
                $group: {
                    _id: "$proprietaire_details._id",
                    totalTrainingHours: {
                        $sum: { $divide: ["$dureeM", 60] }
                    }
                }
            },
            {
                $set: {
                    "proprietaire_details.totalTrainingHours": "$totalTrainingHours"
                }
            },
            {
                $project: {
                    _id: "$proprietaire_details._id",
                    totalTrainingHours: "$proprietaire_details.totalTrainingHours"
                }
            }
        ]);

        if (result.length === 0) {
            return res.status(404).json({ message: "Proprietaire not found or no training hours recorded" });
        }

        res.json(result[0]);
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});

module.exports = router;
