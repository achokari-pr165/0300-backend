const mongoose = require('mongoose')
const express = require('express')
const router = express.Router()

router.use(express.urlencoded({extended:true}))
router.use(express.json())

const ecoleModel = require('../models/ecoleModel')


// Route Post/Create
router.post('/', async (req, res) => {
    const { adresse, nom, localite, npa, moniteurs } = req.body;
    const nouvelleEcole = new ecoleModel({
        adresse: adresse,
        nom: nom,
        localite: localite,
        npa: npa,
        moniteurs: moniteurs,
    });

    try {
        const savedEcole = await nouvelleEcole.save();
        res.status(201).send("Ecole créé");
    } catch (err) {
        res.status(400).send(err.message);
    }
});

// Route Get/Read
router.get('/:id', async (req, res) => {
    const ecoleId = req.params.id;

    try {
        const ecole = await ecoleModel.findById(ecoleId);
        if (!ecole) {
            return res.status(404).json({ message: "Ecole not found" });
        }
        res.json(ecole);
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});






// Route Patch/Update
router.put('/:id', async (req, res) => {
    const ecoleId = req.params.id;

    try {
        const updatedEcole = await ecoleModel.findByIdAndUpdate(ecoleId, req.body, { new: true });
        if (!updatedEcole) {
            return res.status(404).json({ message: "Ecole not found" });
        }
        res.json(updatedEcole);
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});

// Route Delete
router.delete('/:id', async (req, res) => {
    const ecoleId = req.params.id;

    try {
        const deletedEcole = await ecoleModel.findByIdAndDelete(ecoleId);
        if (!deletedEcole) {
            return res.status(404).json({ message: "Cours not found" });
        }
        res.json({ message: "Ecole deleted" });
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});


module.exports = router